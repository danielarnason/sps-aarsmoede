## QGIS + Spatial Suite

---

## QGIS2SPS
Findes på Github<br/>
[QGIS2SPS]( https://github.com/spatialsuite/qgis2sps )

---

## Hvad med SPS2QGIS?

Kan man ikke åbne temaer fra SpS i QGIS?

+++

## Man skal vide følgende

1. Temafil
	* Navn på datasource
2. Datasource
	* Navn på database schema og tabel
1. Endpoint
	* Forbindelses parametre til database

+++

## Datasource

```
<datasource displayname="" endpoint="k240" name="ds_skolepunkter">
	<table geometrycolumn="wkb_geometry" name="temakort.skolepunkter" />
</datasource>
```

+++

## Endpoint

```
<endpoint endpointtype="postgis" name="k240">
	<type>postgres</type>
	<connect>localhost:5432</connect>
	<user>ditbrugernavnher</user>
	<password>ditkodeordher</password>
	<table geometrycolumn="wkb_geometry" pkcolumn="ogc_fid" />
</endpoint>
```

+++

![Video](https://www.youtube.com/embed/CAkK6lDzIlA)

---

@fa[github-square]([https://github.com/danielarnason]( danielarnason ))
@fa[linkedin](danielarnason)<br/>
@fa[twitter-square](@danielarnason85)<br/>

